export type TableRowType = {
  id: string;
  name: string;
  surname: string;
  age: number;
  city: string;
};

export type TablesType = {
  id: string;
  data: TableRowType[];
};
