import { TableForm } from './components/TableForm';
import { Tables } from './components/Tables';

function App() {
  return (
    <div className="container">
      <div className="table_row">
        <TableForm />
        <Tables />
      </div>
    </div>
  );
}

export default App;
