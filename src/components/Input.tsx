import { FC } from 'react';
import { ErrorMessage } from '@hookform/error-message';
import { FieldErrors, UseFormRegister, RegisterOptions } from 'react-hook-form';

type InputType = {
  name: string;
  placeholder?: string;
  errors?: FieldErrors<Record<string, string>>;
  register: UseFormRegister<Record<string, any>>;
  validationSchema: RegisterOptions;
  fullWidth?: boolean;
  value?: string | number;
};

export const Input: FC<InputType> = ({
  name,
  placeholder = '',
  errors,
  register,
  validationSchema,
  fullWidth,
  value,
}) => {
  return (
    <div className="input">
      <input
        className={`input__item ${fullWidth ? 'fullWidth' : ''}`}
        placeholder={placeholder}
        onFocus={(e) => (e.target.placeholder = '')}
        {...register(name, validationSchema)}
        onBlur={(e) => (e.target.placeholder = placeholder)}
        defaultValue={value}
      />
      <ErrorMessage
        errors={errors}
        name={name}
        render={({ message }) => (
          <div className="input__error">
            {errors && errors[name]?.type === 'required'
              ? 'Field is required'
              : message}
          </div>
        )}
      />
    </div>
  );
};
