import { FC, useState } from 'react';
import Modal from 'react-modal';

import { useTablesStore } from '../store/tables';
import { TableRowType } from '../types/tables';
import { Button } from './Button';
import { TableForm } from './TableForm';

Modal.setAppElement('#root');

type TableType = {
  id: string;
  data: TableRowType[];
};

type ActiveTableDataType = {
  tableId: string;
  rowData: TableRowType;
};

export const Table: FC<TableType> = ({ id, data }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [activeTableData, setActiveTableData] = useState<ActiveTableDataType>();
  const deleteRow = useTablesStore((state) => state.deleteRow);
  const copyTable = useTablesStore((state) => state.copyTable);
  const deleteTable = useTablesStore((state) => state.deleteTable);

  const closeModal = () => {
    setModalOpen(false);
  };

  const renderRows = () => {
    return data.map((d) => {
      return (
        <tr key={d.id}>
          <td>{d.name}</td>
          <td>{d.surname}</td>
          <td>{d.age}</td>
          <td>{d.city}</td>
          <td className="table__buttons">
            <Button
              title="Edit"
              onClick={() => {
                setModalOpen(true);
                setActiveTableData({ tableId: id, rowData: d });
              }}
              view="link"
            />
            <Button
              title="Delete"
              onClick={() => deleteRow(id, d.id)}
              view="link"
              customClasses="color_danger"
            />
          </td>
        </tr>
      );
    });
  };
  return (
    <>
      <div className="table_wrap">
        <div className="table_header">
          <div className="right">
            <Button
              title="Copy table"
              view="small"
              onClick={() => copyTable(id)}
            />
            <i className="close" onClick={() => deleteTable(id)}></i>
          </div>
        </div>
        <table className="table">
          <thead>
            <tr>
              <td>Name</td>
              <td>Surname</td>
              <td>Age</td>
              <td>City</td>
              <td></td>
            </tr>
          </thead>
          <tbody>{renderRows()}</tbody>
        </table>
      </div>
      <Modal
        isOpen={modalOpen}
        onRequestClose={closeModal}
        className="modal"
        overlayClassName="overlay"
      >
        <TableForm
          values={activeTableData?.rowData}
          tableId={activeTableData?.tableId}
          onSubmit={() => closeModal()}
          edit
        />
      </Modal>
    </>
  );
};
