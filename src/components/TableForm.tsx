import { FC } from 'react';
import { useForm, Controller } from 'react-hook-form';
import uuid from 'react-uuid';
import { useTablesStore } from '../store/tables';
import { TableRowType } from '../types/tables';
import { Button } from './Button';
import { Input } from './Input';
import { Select } from './Select';

const cities = [
  {
    label: 'Riga',
    value: 'Riga',
  },
  {
    label: 'Dpils',
    value: 'Dpils',
  },
  {
    label: 'Liepaja',
    value: 'Liepaja',
  },
];

type TableFormType = {
  values?: TableRowType | null;
  tableId?: string;
  onSubmit?: () => void;
  edit?: boolean;
};

export const TableForm: FC<TableFormType> = ({
  values,
  tableId = '',
  onSubmit,
  edit,
}) => {
  const {
    register,
    control,
    handleSubmit,
    formState: { isDirty, errors, isSubmitted },
    reset,
  } = useForm();
  const addRow = useTablesStore((state) => state.addRow);
  const editRow = useTablesStore((state) => state.editRow);

  return (
    <form
      className="table_form"
      onSubmit={handleSubmit((data) => {
        const newData = data as TableRowType;

        if (edit) {
          editRow(tableId, values?.id || '', newData);
        } else {
          newData.id = uuid();
          addRow(newData);
        }

        onSubmit && onSubmit();
        reset();
      })}
    >
      <Input
        name="name"
        placeholder="Name"
        errors={errors}
        register={register}
        validationSchema={{
          required: true,
        }}
        fullWidth
        value={values?.name}
      />
      <br />

      <Input
        name="surname"
        placeholder="Surname"
        errors={errors}
        register={register}
        validationSchema={{
          required: true,
        }}
        fullWidth
        value={values?.surname}
      />
      <br />

      <Input
        name="age"
        placeholder="Age"
        errors={errors}
        register={register}
        validationSchema={{
          required: true,
          valueAsNumber: true,
          validate: (value: number) =>
            value > 0 ? true : 'Should be a number',
        }}
        fullWidth
        value={values?.age}
      />
      <br />
      <Controller
        control={control}
        name="city"
        rules={{ required: true }}
        defaultValue={values?.city}
        render={({ field: { onChange, name }, formState: { errors } }) => (
          <Select
            options={cities}
            placeholder="City"
            onSelect={onChange}
            errors={errors}
            name={name}
            reset={isSubmitted}
            value={values?.city}
          />
        )}
      />
      <br />
      <Button title="Submit" type="submit" disabled={!isDirty} fullWidth />
    </form>
  );
};
