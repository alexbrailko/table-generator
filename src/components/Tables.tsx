import { useTablesStore } from '../store/tables';
import { Table } from './Table';

export const Tables = () => {
  const tables = useTablesStore((state) => state.tables);

  const renderTable = () => {
    return tables.map((table) => (
      <Table key={table.id} data={table.data} id={table.id} />
    ));
  };

  return <>{renderTable()}</>;
};
