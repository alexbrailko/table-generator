import { FC } from 'react';

type ButtonType = {
  title: string;
  type?: React.ButtonHTMLAttributes<HTMLButtonElement>['type'];
  disabled?: boolean;
  onClick?: () => void;
  fullWidth?: boolean;
  view?: 'link' | 'small';
  customClasses?: string;
};

export const Button: FC<ButtonType> = ({
  title,
  type = undefined,
  disabled = false,
  onClick,
  fullWidth,
  view,
  customClasses = '',
}) => {
  return (
    <button
      className={`button ${
        fullWidth ? 'fullWidth' : ''
      } ${view} ${customClasses}`}
      type={type}
      disabled={disabled}
      onClick={onClick}
    >
      {title}
    </button>
  );
};
