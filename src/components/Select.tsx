import { FC, useEffect, useState } from 'react';
import { ErrorMessage } from '@hookform/error-message';
import { FieldErrors } from 'react-hook-form';

type OptionType = {
  label: string;
  value: string;
};

type SelectType = {
  placeholder?: string;
  options: OptionType[];
  onSelect: (value: string) => void;
  errors?: FieldErrors<Record<string, string>>;
  name?: string;
  reset?: boolean;
  value?: string;
};

export const Select: FC<SelectType> = ({
  placeholder,
  options,
  onSelect,
  errors,
  name = '',
  reset = false,
  value,
}) => {
  const [active, setActive] = useState(false);
  const [selectedValue, setSelectedValue] = useState<OptionType | null>(null);

  useEffect(() => {
    if (reset) {
      setSelectedValue(null);
    }
  }, [reset]);

  useEffect(() => {
    if (value) {
      const option = options.filter((option) => option.value === value);
      if (option.length) {
        setSelectedValue(option[0]);
      }
    }
  }, [value, options]);

  const onOptionSelect = (val: OptionType) => {
    setActive(false);
    onSelect(val.value);
    setSelectedValue(val);
  };

  return (
    <div className="select">
      <div
        className={`select__input ${active ? 'active' : ''}`}
        onClick={() => setActive(active ? false : true)}
      >
        {!selectedValue?.label ? placeholder || 'Select' : selectedValue.label}
      </div>
      <div className={`select__options ${active ? 'active' : ''}`}>
        {options.length &&
          options.map((option) => (
            <div
              key={option.value}
              className={`select__options__option ${
                selectedValue?.value === option.value ? 'current' : ''
              }`}
              onClick={() => onOptionSelect(option)}
            >
              {option.label}
            </div>
          ))}
      </div>
      <ErrorMessage
        errors={errors}
        name={name}
        render={({ message }) => (
          <div className="input__error">
            {errors && errors[name]?.type === 'required'
              ? 'Field is required'
              : message}
          </div>
        )}
      />
    </div>
  );
};
