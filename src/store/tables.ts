import create from 'zustand';
import uuid from 'react-uuid';
import { TableRowType, TablesType } from '../types/tables';

interface TablesState {
  tables: TablesType[];
  addRow: (data: TableRowType) => void;
  editRow: (tableId: string, rowId: string, data: TableRowType) => void;
  deleteRow: (tableId: string, rowId: string) => void;
  copyTable: (tableId: string) => void;
  deleteTable: (tableId: string) => void;
}

// const defaultTableRow = {
//   id: uuid(),
//   name: 'a',
//   surname: 'b',
//   age: 10,
//   city: 'c',
// };

const defaultTable = {
  id: uuid(),
  data: [],
};

export const useTablesStore = create<TablesState>((set, get) => ({
  tables: [defaultTable],
  addRow: (data: TableRowType) => {
    //only for first table
    const tables = get().tables;

    tables[0].data = [...tables[0].data, data];

    set(() => ({
      tables: [...tables],
    }));
  },
  editRow: (tableId: string, rowId: string, data: TableRowType) => {
    const tables = get().tables;
    const findTableIndex = tables.findIndex((table) => tableId === table.id);
    const updateRow = tables[findTableIndex].data.map((r) =>
      r.id === rowId ? { ...r, ...data } : r,
    );
    tables[findTableIndex].data = updateRow;

    set(() => ({
      tables: [...tables],
    }));
  },
  deleteRow: (tableId: string, rowId: string) => {
    const tables = get().tables;
    const findTableIndex = tables.findIndex((table) => tableId === table.id);
    const updateRow = tables[findTableIndex].data.filter((t) => t.id !== rowId);
    tables[findTableIndex].data = updateRow;

    set(() => ({
      tables: [...tables],
    }));
  },
  copyTable: (tableId: string) => {
    const table = get().tables.find(
      (table) => tableId === table.id,
    ) as TablesType;
    const newTable = { ...table };
    newTable.id = uuid();

    set((state) => ({
      tables: [...state.tables, newTable],
    }));
  },
  deleteTable: (tableId: string) => {
    const tables = get().tables;
    const first = { ...tables[0] };
    //remove first
    tables.shift();
    const filtered = tables.filter((table) => table.id !== tableId);

    set(() => ({
      tables: [first, ...filtered],
    }));
  },
}));
